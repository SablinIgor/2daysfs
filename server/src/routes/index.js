const routes = require('express').Router();
const controllerDB = require('../controllers/db/dynamodb')

routes.post('/register', (req, res) => {
    res.status(200).json({ message: `Hello ${req.body.email}! Your user was registered! Have fun!` });
});

routes.post('/new', (req, res) => {
    console.log(req.body)
    controllerDB.saveTour(req.body).then((data) => {
        res.send(data)
    })
})

routes.get('/tours', (req, res) => {
    controllerDB.scan().then((data) => {
        res.send(data)
    })

})

routes.post('/tours/search/:tour_name', (req, res) => {

    console.log(`param: ${req.params.tour_name}`)
    controllerDB.getTourByName(req.params.tour_name).then((data) => {
        res.send(data)
    })

})

routes.post('/tours/:id', (req, res) => {

    controllerDB.getTourById(req.params.id).then((data) => {
        res.send(data)
    })
})




module.exports = routes;
