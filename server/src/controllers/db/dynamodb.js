const AWS = require("aws-sdk");
const dotenv = require('dotenv');

dotenv.config();

console.log(`Your AWS_END_POINT is ${process.env.AWS_END_POINT}`)

if (process.env.AWS_END_POINT !== undefined) {
    AWS.config.update({
        region: "us-east-2",
        endpoint: process.env.AWS_END_POINT
    });
}

let docClient = new AWS.DynamoDB.DocumentClient();

exports.scan = async () => {
    const params = {
        TableName: "local.Tour"
    };

    try {
        const { Items } = await docClient.scan(params).promise();
        return {
            success: true,
            message: 'Loaded data',
            tours: Items
        }
    } catch (e) {
        return {
            success: false,
            message: 'Error: Server error'
        }
    }
}

exports.saveTour = async (newTour) => {
    const params = {
        TableName: "local.Tour",
        Item: newTour
    }

    try {
        await docClient.put(params).promise();

        return {
            success: true,
            message: 'Tour saved!'
        }

    } catch (e) {
        return {
            success: false,
            message: 'Error: Server error'
        }
    }
}

exports.getTourById = async (tourId) => {
    const params = {
        TableName: "local.Tour",
        KeyConditionExpression: "#id = :tour_id",
        ExpressionAttributeNames:{
            "#id": "tour_id"
        },
        ExpressionAttributeValues: {
            ":tour_id": tourId
        }
    };

    try {
        let { Items } = await docClient.query(params).promise();
        console.log(Items)

        return {
            success: true,
            message: 'Loaded data',
            tours: Items
        }
    } catch (e) {
        return {
            success: false,
            message: 'Error: Server error'
        }
    }
}

exports.getTourByName = async (tourName) => {
    let params = {
        TableName: "local.Tour",
        IndexName: 'city_index',
        KeyConditionExpression: "#tour_name = :city",
        ExpressionAttributeNames:{
            "#tour_name": "city"
        },
        ExpressionAttributeValues: {
            ":city": tourName
        }
    };

    try {
        let { Items } = await docClient.query(params).promise();

        console.log(`Items found: ${Items.length}`)

        if ( Items.length === 0) { // Если не нашли по имени города, то ищем по имени страны
            console.log(`Search country`)
            params = {
                TableName: "local.Tour",
                IndexName: 'country_index',
                KeyConditionExpression: "#tour_name = :country",
                ExpressionAttributeNames:{
                    "#tour_name": "country"
                },
                ExpressionAttributeValues: {
                    ":country": tourName
                }
            };

            ({ Items } = await docClient.query(params).promise())

            console.log(`Items found: ${Items.length}`)
        }

        return {
            success: true,
            message: 'Loaded data',
            tours: Items
        }
    } catch (e) {
        return {
            success: false,
            message: 'Error: Server error'
        }
    }
}
