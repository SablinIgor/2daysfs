const AWS = require("aws-sdk");
const fs = require('fs');

AWS.config.update({
    region: "us-east-2"/*,
    endpoint: "http://localhost:8000"*/
});

let docClient = new AWS.DynamoDB.DocumentClient();

console.log('Importing tours into DynamoDB. Please wait.');

const allTours = JSON.parse(fs.readFileSync('./tour.json', 'utf8'));
allTours.forEach(function(tour) {
    let params = {
        TableName: "local.Tour",
        Item: {
            "tour_id": tour.tour_id,
            "country": tour.country,
            "city": tour.city,
            "user_id": tour.user_id,
            "attributes": tour.attributes
        }
    };

    docClient.put(params, function(err, data) {
    if (err) {
      console.error("Unable to add tour", tour.tour_id, ". Error JSON:", JSON.stringify(err, null, 2));
    } else {
      console.log("PutItem succeeded:", tour.tour_id);
    }
  });
});
