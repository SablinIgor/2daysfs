const AWS = require("aws-sdk");

AWS.config.update({
    region: "us-east-2",
    endpoint: "http://localhost:8000"
});

let docClient = new AWS.DynamoDB.DocumentClient();

console.log('Query tour from DynamoDB. Please wait.');

const params = {
    TableName: "local.Tour",
    KeyConditionExpression: "#id = :tour_id",
    ExpressionAttributeNames:{
        "#id": "tour_id"
    },
    ExpressionAttributeValues: {
        ":tour_id": "24203636-f03d-487e-8ced-2f335afe9b8a"
    }
};

docClient.query(params, function(err, data) {
    if (err) {
        console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
    } else {
        console.log("Query succeeded.");
        data.Items.forEach(function(item) {
            console.log("item", item.attributes.Places);
        });
    }
});
