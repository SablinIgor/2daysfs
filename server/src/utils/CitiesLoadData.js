var AWS = require("aws-sdk");
var fs = require('fs');

AWS.config.update({
    region: "us-east-2",
    endpoint: "http://localhost:8000"
});

var docClient = new AWS.DynamoDB.DocumentClient();

console.log("Importing cities into DynamoDB. Please wait.");

var allCities = JSON.parse(fs.readFileSync('./city.json', 'utf8'));
allCities.forEach(function(city) {
  var params = {
    TableName: "local.City",
    Item: {
      "country_id":  city.country_id,
      "city_id":  city.city_id,
      "region_id":  city.region_id,
      "name": city.name
    }
  };

  docClient.put(params, function(err, data) {
    if (err) {
      console.error("Unable to add city", city.name, ". Error JSON:", JSON.stringify(err, null, 2));
    } else {
      console.log("PutItem succeeded:", city.name);
    }
  });
});
