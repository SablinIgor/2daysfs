var AWS = require("aws-sdk");
var fs = require('fs');

AWS.config.update({
    region: "us-east-2",
    endpoint: "http://localhost:8000"
});

var docClient = new AWS.DynamoDB.DocumentClient();

console.log("Importing countries into DynamoDB. Please wait.");

var allCountries = JSON.parse(fs.readFileSync('./country.json', 'utf8'));
allCountries.forEach(function(country) {
  var params = {
    TableName: "local.Country",
    Item: {
      "country_id":  country.country_id,
      "name": country.name
    }
  };

  docClient.put(params, function(err, data) {
    if (err) {
      console.error("Unable to add country", country.name, ". Error JSON:", JSON.stringify(err, null, 2));
    } else {
      console.log("PutItem succeeded:", country.name);
    }
  });
});
