var AWS = require("aws-sdk");

AWS.config.update({
    region: "us-east-2" /*,
    endpoint: "http://localhost:8000"*/
});

var dynamodb = new AWS.DynamoDB();

var params = {
    TableName : "local.Tour",
    KeySchema: [
        { AttributeName: "tour_id", KeyType: "HASH"}
    ],

    AttributeDefinitions: [
        { AttributeName: "tour_id", AttributeType: "S" },
        { AttributeName: "country", AttributeType: "S" },
        { AttributeName: "city", AttributeType: "S" },
        { AttributeName: "user_id", AttributeType: "S" }
    ],

    GlobalSecondaryIndexes: [
        {
            IndexName: 'country_index',
            KeySchema: [
                {
                    AttributeName: 'country',
                    KeyType: 'HASH',
                }
                ],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: {
                ReadCapacityUnits: 1,
                WriteCapacityUnits: 1
            }
        },
        {
            IndexName: 'city_index',
            KeySchema: [
                {
                    AttributeName: 'city',
                    KeyType: 'HASH',
                }
            ],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: {
                ReadCapacityUnits: 1,
                WriteCapacityUnits: 1
            }
        },
        {
            IndexName: 'user_id_index',
            KeySchema: [
                {
                    AttributeName: 'user_id',
                    KeyType: 'HASH',
                }
            ],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: {
                ReadCapacityUnits: 1,
                WriteCapacityUnits: 1
            }
        },

     ],

    ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1
    }
};

dynamodb.createTable(params, function(err, data) {
    if (err) {
        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
    }
});
