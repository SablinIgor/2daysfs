import { HTTP } from './Api'

export default {
  register (credentials) {
    return HTTP.post('register', credentials)
  }
}
