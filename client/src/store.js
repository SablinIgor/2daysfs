import Vue from 'vue'
import Vuex from 'vuex'
import { Auth } from 'aws-amplify'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
  key: '2days',
  storage: localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    signedIn: false,
    user: {}
  },
  getters: {
    SIGNEDIN: state => {
      return state.signedIn
    },
    USER_INFO: state => {
      return state.user
    }
  },
  mutations: {
    SET_SIGNEDIN: (state, payload) => {
      state.signedIn = payload
    },
    SET_USER_INFO: (state, payload) => {
      state.user = payload
    },
    CLEAR_USER_INFO: (state, payload) => {
      state.user = payload
    }
  },
  actions: {
    SET_SIGNEDIN: (context, payload) => {
      context.commit('SET_SIGNEDIN', payload)
    },
    SET_USER_INFO: async (context) => {
      const user = await Auth.currentAuthenticatedUser()
      context.commit('SET_USER_INFO', user)
    },
    CLEAR_USER_INFO: (context, payload) => {
      context.commit('CLEAR_USER_INFO', payload)
    }
  },
  plugins: [vuexPersist.plugin]

})
