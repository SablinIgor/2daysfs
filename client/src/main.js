import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Amplify, * as AmplifyModules from 'aws-amplify'
import { AmplifyPlugin } from 'aws-amplify-vue'
import { I18n } from 'aws-amplify'
import aws_exports from './aws-exports'
import App from './App.vue'
import router from './router'
import store from './store'

Amplify.configure(aws_exports)

const dict = {
  'ru': {
    'Username': 'Логин',
    'Enter your username': 'Введите ваш логин',
    'Password': 'Пароль',
    'Enter your password': 'Введите ваш пароль',
    'Forget your password? ': 'Забыли пароль? ',
    'Reset password': 'Сбросить пароль',
    'Sign In': 'Войти',
    'No account? ': 'Нет логина? ',
    'Create account': 'Создайте сейчас',
    'Sign In Account': 'Добро пожаловать!',

    'Sign Out': 'Выйти',

    'Have an account? ': 'Есть логин? ',
    'Sign Up Account': 'Регистрация',
    'Phone Number': 'Номер телефона',

    'Code': 'Код',
    'Confirm': 'Подтвердить',
    'Back to Sign In': 'Войти снова',

    'Confirmation Code': 'Код подтверждения',
    'Lost your code? ': 'Код утерян? ',
    'Resend Code': 'Послать код снова',
    'Confirm Sign Up': 'Подтверждение',

    'New Password': 'Новый пароль',
    'Send Code': 'Послать код',
    'Submit': 'Подтверждение',
    'Reset your password': 'Сбросить пароль',

    'Enter new password': 'Ввести новый пароль'
  }
}

I18n.putVocabularies(dict)

I18n.setLanguage('ru')

Vue.use(AmplifyPlugin, AmplifyModules)

Vue.config.productionTip = false

Vue.use(Vuetify)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
