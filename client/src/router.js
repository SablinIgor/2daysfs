import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Register from './views/Register'
import Tour from './components/Tour'
import ShowCase from './views/ShowCase'
import Details from './views/Details'
import NewTour from './views/NewTour'
import store from './store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/new',
      name: 'new-tour',
      component: NewTour,
      meta: { requiresAuth: true }
    },
    {
      path: '/search',
      name: 'search',
      component: ShowCase,
      props: (route) => ({ query: route.query.name })
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/tour',
      name: 'tour',
      component: Tour
    },
    {
      path: '/details',
      name: 'details',
      component: Details,
      props: (route) => ({ id: route.query.id })
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (store.getters.SIGNEDIN !== true) {
      next({
        path: '/register',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
