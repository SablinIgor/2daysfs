module.exports = {
  pluginOptions: {
    s3Deploy: {
      awsProfile: 'default',
      region: 'us-east-2',
      bucket: '2days-frontend-stage',
      createBucket: false,
      staticHosting: true,
      staticIndexPage: 'index.html',
      staticErrorPage: 'error.html',
      assetPath: 'dist',
      assetMatch: '**',
      deployPath: '/',
      acl: 'public-read',
      pwa: false,
      enableCloudfront: false,
      uploadConcurrency: 5,
      pluginVersion: '3.0.0'
    }
  }
}
